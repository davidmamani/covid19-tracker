pipeline {
    agent any

    triggers {
        pollSCM('H/2 * * * *')
    }

    environment {
        DOCKER_ACCOUNT = credentials('robot_account')
        DOCKER_URL = 'docker.jala.pro'
        DOCKER_TAG = "${env.GIT_COMMIT}"
        DEV_HOST_SERVER = "ssh://ubuntu@10.24.48.143"
        DEV_ENV = 'dev'
    }
    stages {
        stage ('Commit') {
            steps {
                sh "docker build -t ${DOCKER_URL}/devops/dm_covid19-tracker:${DOCKER_TAG} ."
            }
        }
        stage ('Publish') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker login -u "${DOCKER_ACCOUNT_USR}" -p "${DOCKER_ACCOUNT_PSW}" "${DOCKER_URL}"'
                sh "docker push ${DOCKER_URL}/devops/dm_covid19-tracker:${DOCKER_TAG}"
            }
            post {
                always {
                    sh "docker image rm -f ${DOCKER_URL}/devops/dm_covid19-tracker:${DOCKER_TAG}"
                }
            }
        }
        stage ('Deploy') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker context create ${DEV_ENV} --docker "host=${DEV_HOST_SERVER}"'
                sh 'docker --context ${DEV_ENV} stack deploy -c docker-compose.dev.yml covid19'
            }
            post {
                always {
                    sh 'docker context rm ${DEV_ENV}'
                }
            }
        }
        stage('Acceptance') {
            when {
                branch 'master'
            }
            steps {
                build job: "demo-ui-acceptance/master", propagate: true, wait: true
            }
        }

        stage('Capacity') {
            when {
                branch 'master'
            }
            steps {
                build job: "gatling-gradle-plugin-demo/master", propagate: true, wait: true
            }
        }
    }
}
